/*
 * Copyright (c) 2010-2017 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.testing.itest;

import java.util.ArrayList;
import java.util.List;

import org.amdatu.mongo.MongoDBService;

class TestUtil {

    private TestUtil() {
        // Not for instantiation...
    }

    public static List<String> asList(Iterable<String> iterable) {
        List<String> result = new ArrayList<>();
        iterable.forEach(name -> result.add(name));
        return result;
    }

    public static String createFilter(String dbName) {
        return "(&(objectClass=" + MongoDBService.class.getName() + ")(dbName=" + dbName + "))";
    }
}
