/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.testing.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestUtils.getBundleContext;
import static org.amdatu.testing.configurator.TestUtils.getService;
import static org.amdatu.mongo.testing.OSGiMongoTestConfigurator.configureMongoDb;
import static org.amdatu.mongo.testing.itest.TestUtil.asList;
import static org.amdatu.mongo.testing.itest.TestUtil.createFilter;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.mongo.MongoDBService;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

import com.mongodb.client.MongoDatabase;

public class TempMongoDBTest {

    /**
     * Tests that the configuration of MongoDB works as expected and that the database that is created goes away when cleaning up.
     * <p>
     * This should test AMDATUTEST-5 and AMDATUTEST-24?!
     * </p>
     */
    @Test
    public void testCreateTempMongoDB() throws Exception {
        AtomicReference<String> dbName = new AtomicReference<>();

        CountDownLatch addedLatch = new CountDownLatch(1);
        CountDownLatch removedLatch = new CountDownLatch(1);

        ServiceListener addedListener = event -> {
            if (event.getType() == ServiceEvent.REGISTERED) {
                dbName.set((String) event.getServiceReference().getProperty("dbName"));

                addedLatch.countDown();
            }
        };

        ServiceListener removedListener = event -> {
            if (event.getType() == ServiceEvent.UNREGISTERING) {
                removedLatch.countDown();
            }
        };

        BundleContext bc = getBundleContext(getClass());
        bc.addServiceListener(addedListener, createFilter("*TempMongoDBTest"));

        configure(this)
            .add(configureMongoDb().setCreateTemporaryDatabase().setTimeout(10, TimeUnit.SECONDS).setAlias("test"))
            .apply();

        try {
            assertTrue("MongoDBService should be registered!", addedLatch.await(10, TimeUnit.SECONDS));
            assertNotNull("We should have got a dbName?!", dbName.get());

            bc.removeServiceListener(addedListener);

            MongoDBService srv = getService(MongoDBService.class, createFilter("*TempMongoDBTest"));

            // Do something with the temporary database otherwise it is not even created...
            MongoDatabase db = srv.getDatabase();
            db.createCollection("test");

            List<String> names = asList(db.listCollectionNames());
            assertTrue("There should be a collection named 'test': " + names, names.contains("test"));

            // We want to be informed of the unregistration of the exact same MongoDBService as was created for us...
            bc.addServiceListener(removedListener, createFilter(dbName.get()));
        } finally {
            cleanUp(this);
        }

        assertTrue("MongoDBService should be unregistered!", removedLatch.await(5, TimeUnit.SECONDS));

        bc.removeServiceListener(removedListener);
    }
}