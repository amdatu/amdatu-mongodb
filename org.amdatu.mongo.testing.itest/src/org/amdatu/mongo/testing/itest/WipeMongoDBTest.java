/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.testing.itest;

import static org.amdatu.mongo.testing.OSGiMongoTestConfigurator.configureMongoDb;
import static org.amdatu.mongo.testing.itest.TestUtil.asList;
import static org.amdatu.mongo.testing.itest.TestUtil.createFilter;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestUtils.getBundleContext;
import static org.amdatu.testing.configurator.TestUtils.getService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.amdatu.mongo.MongoDBService;
import org.bson.Document;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;

/**
 * Tests that we can use the "wiping" strategy to wipe MongoDB after a cleanup.
 */
public class WipeMongoDBTest {

    @Test
    public void testWipeMongoDB() throws Exception {
        AtomicReference<String> dbName = new AtomicReference<>();

        CountDownLatch addedLatch = new CountDownLatch(1);

        ServiceListener listener = event -> {
            if (event.getType() == ServiceEvent.REGISTERED) {
                dbName.set((String) event.getServiceReference().getProperty("dbName"));

                addedLatch.countDown();
            }
        };

        BundleContext bc = getBundleContext(getClass());
        bc.addServiceListener(listener, createFilter("test-*"));

        configure(this).add(configureMongoDb().setAlias("test").setWipeOrDropCollections()).apply();

        String collectionWithIdxName = "testIdx";
        String collectionWithoutIdxName = "testNoIdx";

        try {
            assertTrue("MongoDBService should be registered!", addedLatch.await(10, TimeUnit.SECONDS));
            assertNotNull("We should have got a dbName?!", dbName.get());

            MongoDBService srv = getService(MongoDBService.class);

            // Do something with the temporary database otherwise it is not even created...
            MongoDatabase db = srv.getDatabase();

            db.createCollection(collectionWithIdxName);
            db.createCollection(collectionWithoutIdxName);

            MongoCollection<Document> collectionWithIdx = db.getCollection(collectionWithIdxName);

            collectionWithIdx.createIndex(new BasicDBObject("key", "text"), new IndexOptions().unique(true));
            collectionWithIdx.insertOne(new Document("key", "value"));

            MongoCollection<Document> collectionWithoutIdx = db.getCollection(collectionWithoutIdxName);

            collectionWithoutIdx.insertOne(new Document("key", "value"));

            List<String> names = asList(db.listCollectionNames());
            assertTrue("There should be a collection named 'testIdx': " + names,
                names.contains(collectionWithIdxName));
            assertTrue("There should be a collection named 'testNoIdx': " + names,
                names.contains(collectionWithoutIdxName));

            assertEquals(1L, collectionWithIdx.countDocuments());
            assertEquals(1L, collectionWithoutIdx.countDocuments());

            assertEquals("key_text", getIndexes(db, collectionWithIdxName) //
                .map(doc -> doc.get("name")) //
                .filter(name -> !"_id_".equals(name)) //
                .findFirst() //
                .orElse(null));
        }
        finally {
            cleanUp(this);
        }

        // The DB itself should still be there, with all of its collections...
        MongoDBService srv = getService(MongoDBService.class);

        MongoDatabase db = srv.getDatabase();
        assertEquals(dbName.get(), db.getName());

        List<String> names = asList(db.listCollectionNames());
        assertTrue("There should still be a collection named 'test': " + names,
            names.contains(collectionWithIdxName));

        configure(this).add(configureMongoDb().setAlias("test").setWipeOrDropCollections()).apply();
        try {
            // The DB itself should still be there, but its collections should be purged...
            srv = getService(MongoDBService.class);

            db = srv.getDatabase();
            assertEquals(dbName.get(), db.getName());

            names = asList(db.listCollectionNames());
            // the collection should be recreated but empty...
            assertTrue("There should still be a collection named 'testIdx': " + names,
                names.contains(collectionWithIdxName));
            assertFalse("There should NOT be a collection named 'testNoIdx': " + names,
                names.contains(collectionWithoutIdxName));

            assertEquals(0L, db.getCollection(collectionWithIdxName).countDocuments());
            assertEquals("key_text", getIndexes(db, collectionWithIdxName) //
                .map(doc -> doc.get("name")) //
                .filter(name -> !"_id_".equals(name)) //
                .findFirst() //
                .orElse(null));
        }
        finally {
            cleanUp(this);
        }
    }

    private Stream<Document> getIndexes(MongoDatabase db, String collectionName) {
        return StreamSupport.stream(db.getCollection(collectionName).listIndexes().spliterator(), false /* parallel */);
    }
}