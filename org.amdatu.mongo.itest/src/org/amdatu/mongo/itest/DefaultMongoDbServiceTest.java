/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.amdatu.mongo.MongoDBService;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class DefaultMongoDbServiceTest {

    private static final String COLLECTION = "demo";
    private volatile MongoDBService m_mongoDbService;
    private volatile BundleContext m_bundlecontext;

    @Before
    public void setUp() {

        configure(this)
            .add(createFactoryConfiguration("org.amdatu.mongo").set("alias", "test"))
            .add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
            .setTimeout(10, TimeUnit.SECONDS)
            .apply();
    }

    @Test
    public void testDefaultMongoDbService() {
        MongoDatabase db = m_mongoDbService.getDatabase();
        assertNotNull(db);
        assertEquals("test", db.getName());
        assertEquals(WriteConcern.ACKNOWLEDGED, db.getWriteConcern());
        assertEquals(ReadPreference.primary(), db.getReadPreference());
    }

    @Test
    public void testCleanupConnections() throws InterruptedException, BundleException {
        TimeUnit.MILLISECONDS.sleep(2000);
        int nrOfConnections = getNrOfConnections();

        for (int i = 0; i < 10; i++) {
            MongoDatabase db = m_mongoDbService.getDatabase();
            assertEquals("test", db.getName());
            db.getCollection(COLLECTION).insertOne(new Document("name", i));

            Bundle bundle = findBundle();
            bundle.stop();
            bundle.start();

            TimeUnit.MILLISECONDS.sleep(1000);
        }
        TimeUnit.MILLISECONDS.sleep(2000);
        assertEquals(nrOfConnections, getNrOfConnections());
    }

    private Bundle findBundle() {
        return Arrays.stream(m_bundlecontext.getBundles()).filter(b -> b.getSymbolicName().equals("org.amdatu.mongo"))
            .findAny().orElseThrow(() -> new RuntimeException("Could not find org.amdatu.mongo bundle."));
    }

    private int getNrOfConnections() {
        MongoDatabase db = m_mongoDbService.getDatabase();
        Document status = db.runCommand(new Document("serverStatus", 1));
        Document currentConnections = (Document) status.get("connections");
        return currentConnections.getInteger("current", 0);
    }

    @After
    public void after() {
        cleanUp(this);
    }
}
