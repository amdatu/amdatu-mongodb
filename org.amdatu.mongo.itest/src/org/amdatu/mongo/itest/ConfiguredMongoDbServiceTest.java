/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.amdatu.mongo.MongoDBService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoDatabase;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ConfiguredMongoDbServiceTest {

    private volatile MongoDBService m_mongoDbService;

    @Before
    public void setUp() throws Exception {
        configure(this)
            .add(createFactoryConfiguration("org.amdatu.mongo")
                    .set("dbName", "mydb1")
                    .set("readPreference", "secondaryPreferred")
                    .set("writeConcern", "ACKNOWLEDGED")
                    .set("alias", "test"))
            .add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
            .apply();
    }

    @Test
    public void testConfiguredMongoDbService() throws Exception {
        MongoDatabase db = m_mongoDbService.getDatabase();
        assertNotNull(db);
        assertEquals("mydb1", db.getName());
        assertEquals(ReadPreference.secondaryPreferred(), db.getReadPreference());
        assertEquals(WriteConcern.ACKNOWLEDGED, db.getWriteConcern());
    }

    @After
    public void after() {
        cleanUp(this);
    }
}
