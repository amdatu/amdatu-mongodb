/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.testing;

import java.util.Optional;

/**
 * MongoDB configuration steps for the OSGiTestConfigurator.
 */
public class OSGiMongoTestConfigurator {

    /**
     * Environment property key used by {@link #configureMongoDb()}. If the property is set (and not empty) it is
     * used to configure the {@code MongoConfigurationStep} host.
     */
    public static final String TESTING_MONGO_HOST = "TESTING_MONGO_HOST";

    /**
     * Environment property key used by {@link #configureMongoDb()}. If the property is set (and not empty) it is
     * used to configure the {@code MongoConfigurationStep} port.
     */
    public static final String TESTING_MONGO_PORT = "TESTING_MONGO_PORT";

    /**
     * Environment property key used by {@link #configureMongoDb()}. If the property is set (and not empty) it is
     * used to configure the {@code MongoConfigurationStep} username.
     */
    public static final String TESTING_MONGO_USERNAME = "TESTING_MONGO_USERNAME";

    /**
     * Environment property key used by {@link #configureMongoDb()}. If the property is set (and not empty) it is
     * used to configure the {@code MongoConfigurationStep} password.
     */
    public static final String TESTING_MONGO_PASSWORD = "TESTING_MONGO_PASSWORD";

    /**
     * Environment property key used by {@link #configureMongoDb()}. If the property is set (and not empty) it is
     * used to configure the {@code MongoConfigurationStep} alias.
     */
    public static final String TESTING_MONGO_ALIAS = "TESTING_MONGO_ALIAS";

    /**
     * Creates a new configuration step that configures an mongo service.
     *
     * @return a new {@link MongoConfigurationStep} step instance.
     */
    public static MongoConfigurationStep configureMongoDb() {
        MongoConfigurationStep step = new MongoConfigurationStep();

        getProperty(TESTING_MONGO_HOST)
            .ifPresent(step::setHost);

        getProperty(TESTING_MONGO_PORT)
            .ifPresent(step::setPort);

        getProperty(TESTING_MONGO_USERNAME)
            .ifPresent(step::setUsername);

        getProperty(TESTING_MONGO_PASSWORD)
            .ifPresent(step::setPassword);

        getProperty(TESTING_MONGO_ALIAS)
            .ifPresent(step::setAlias);

        return step;
    }

    private static Optional<String> getProperty(String key) {
        String value = System.getenv(key);
        return isEmpty(value) ? Optional.empty() : Optional.of(value);
    }

    private static boolean isEmpty(String s) {
        return s == null || "".equals(s);
    }

    private OSGiMongoTestConfigurator() {
        // Not meant for instantiation...
    }
}
