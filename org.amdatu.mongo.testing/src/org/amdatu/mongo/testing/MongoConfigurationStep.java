/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.testing;

import static org.amdatu.testing.configurator.TestUtils.getBundleContext;
import static org.amdatu.testing.configurator.TestUtils.getFactoryConfiguration;
import static org.osgi.framework.Constants.OBJECTCLASS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.testing.configurator.TestConfigurationException;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;

/**
 * A configuration step that cleans up all collections from MongoDB when
 * applying this configuration step.
 * <p>
 * Which collections are wiped or dropped is configurable, and by default keeps
 * a small amount of collections that are important to IG.
 * </p>
 */
public class MongoConfigurationStep implements ConfigurationStep {

    /**
     * Creates a new temporary database prior to each test case and drops it
     * afterwards.
     */
    class TempDBConfigurationStep implements ConfigurationStep {
        private String m_dbName;
        private Configuration m_configuration;

        TempDBConfigurationStep() {
            // Nop
        }

        @Override
        public void apply(Object testCase) {
            m_dbName = String.format("test-%d-%s", System.currentTimeMillis(), testCase.getClass().getSimpleName());

            m_configuration = getFactoryConfiguration(PID);
            try {
                m_configuration.update(createConfig(m_dbName));
            }
            catch (IOException e) {
                throw new TestConfigurationException("Could not configure MongoDB: update of configuration failed!", e);
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            if (m_configuration == null) {
                // Nothing to do: apply was never called!
                return;
            }

            BundleContext ctx = getBundleContext(testCase.getClass());

            // Stage 1: drop the DB to clean up our mess...
            Collection<ServiceReference<MongoDBService>> refs;
            try {
                refs = ctx.getServiceReferences(MongoDBService.class, createAliasFilterString(m_alias));
            }
            catch (InvalidSyntaxException e) {
                throw new RuntimeException("Invalid filter syntax!", e);
            }

            if (!refs.isEmpty()) {
                dropDatabase(ctx, refs.iterator().next());
            }

            // Stage 2: wait until the service itself is gone after we've deleted its
            // configuration...
            CountDownLatch removedLatch = new CountDownLatch(1);

            ServiceListener listener = event -> {
                if (event.getType() == ServiceEvent.UNREGISTERING) {
                    removedLatch.countDown();
                }
            };

            try {
                ctx.addServiceListener(listener, createAliasFilterString(m_alias));
            }
            catch (InvalidSyntaxException e) {
                throw new RuntimeException("Invalid filter syntax?!", e);
            }

            String pid = m_configuration.getPid();

            try {
                // This is asynchronous, so we cannot be sure the MongoDBService instance is
                // gone and we have to wait for the service to go away...
                m_configuration.delete();

                // Only fail if the database was there, but we did not see it going away within
                // the set time boundaries...
                if (!removedLatch.await(m_duration, m_unit)) {
                    throw new TestConfigurationException("Could not remove MongoDBService with pid " + pid + " within "
                        + m_duration + " " + m_unit.name().toLowerCase());
                }
            }
            catch (IOException | InterruptedException e) {
                throw new TestConfigurationException("Problem doing cleanup for MongoDBService with pid " + pid, e);
            }
            finally {
                ctx.removeServiceListener(listener);
                // We're done with this configuration...
                m_configuration = null;
            }
        }

        private void dropDatabase(BundleContext ctx, ServiceReference<MongoDBService> ref) {
            MongoDBService srv = ctx.getService(ref);
            try {
                if (srv == null) {
                    // We're done already: service is no longer with us...
                    return;
                }

                try {
                    srv.getDatabase().drop();
                }
                catch (Exception e) {
                    // Print the trace, but we cannot bail out here...
                    System.err.println("[WARNING] Failed to drop MongoDB database: " + m_dbName);
                    e.printStackTrace();
                }
            }
            finally {
                ctx.ungetService(ref);
            }
        }
    }

    /**
     * Drops or wipes all collections prior to each test case.
     */
    class WipeDBConfigurationStep implements ConfigurationStep {

        WipeDBConfigurationStep() {
            // Nop
        }

        @Override
        public void apply(Object testCase) {
            // Add a shutdown hook, since Bnd does not shutdown the framework
            // gracefully and we want to drop the database when we're finished.
            if (SHUTDOWN_HOOK_CREATED.compareAndSet(false, true)) {
                TEMP_DB_NAME.set(String.format("test-%d", System.currentTimeMillis()));

                Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                    BundleContext ctx = getBundleContext(MongoConfigurationStep.class);

                    Collection<ServiceReference<MongoDBService>> refs;
                    try {
                        refs = ctx.getServiceReferences(MongoDBService.class, createAliasFilterString(m_alias));
                    } catch (InvalidSyntaxException e) {
                        throw new RuntimeException("Invalid filter syntax!", e);
                    }

                    if (refs != null && refs.size() > 0) {
                        ServiceReference<MongoDBService> ref = refs.iterator().next();
                        MongoDBService srv = ctx.getService(ref);
                        if (srv != null) {
                            srv.getDatabase().drop();
                        }
                    }
                }));
            }
            // check if there's a Mongo service present already...
            BundleContext ctx = getBundleContext(getClass());

            Collection<ServiceReference<MongoDBService>> refs;
            try {
                refs = ctx.getServiceReferences(MongoDBService.class, createAliasFilterString(m_alias));
            }
            catch (InvalidSyntaxException e) {
                throw new RuntimeException("Invalid filter syntax!", e);
            }

            if (refs.isEmpty()) {
                // No MongoDB service present for our DB, let's configure one...
                configureMongoDBService();
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            BundleContext ctx = getBundleContext(getClass());

            Collection<ServiceReference<MongoDBService>> refs;
            try {
                refs = ctx.getServiceReferences(MongoDBService.class, createAliasFilterString(m_alias));
            }
            catch (InvalidSyntaxException e) {
                throw new RuntimeException("Invalid filter syntax!", e);
            }

            if (!refs.isEmpty()) {
                // A MongoDB service is present, let's clean it up...
                dropOrWipeCollections(ctx, refs.iterator().next());
            }
        }

        private void configureMongoDBService() {
            Configuration configuration = getFactoryConfiguration(PID);
            try {
                configuration.update(createConfig(TEMP_DB_NAME.get()));
            }
            catch (IOException e) {
                throw new RuntimeException("Could not configure Mongo", e);
            }
        }

        private void dropOrWipeCollection(MongoDatabase db, String name) {
            if (m_excludedCollections.contains(name)) {
                System.err.println("[DEBUG] Ignoring collection " + name + " ...");

                return;
            }
            MongoCollection<Document> collection = db.getCollection(name);
            if (m_wipedCollections.contains(name)) {
                System.err.println("[DEBUG] Wiping collection " + name + " ...");

                collection.deleteMany(new BasicDBObject());
            }
            else {
                List<Document> indexInfo = new ArrayList<>();
                ((Iterable<Document>) collection.listIndexes()).forEach(indexInfo::add);

                System.err.println("[DEBUG] Dropping collection " + name + " ...");

                collection.drop();

                indexInfo.stream() //
                    .filter(idx -> !"_id_".equals(idx.get(NAME))) //
                    .forEach(idx -> {
                        IndexOptions indexOptions = new IndexOptions();
                        indexOptions.name((String) idx.get(NAME));
                        indexOptions.unique(Boolean.TRUE.equals(idx.get(UNIQUE)));
                        indexOptions.sparse(Boolean.TRUE.equals(idx.get(SPARSE)));
                        indexOptions.languageOverride((String) idx.get("language_override"));
                        indexOptions.defaultLanguage((String) idx.get("default_language"));
                        indexOptions.textVersion((Integer) idx.get("textIndexVersion"));
                        indexOptions.version((Integer) idx.get("v"));
                        indexOptions.weights((Bson) idx.get("weights"));

                        try {
                            System.err.println("[DEBUG] Recreating index " + idx.get(NAME) + " ...");

                            collection.createIndex((Bson) idx.get("key"), indexOptions);
                        }
                        catch (MongoException e) {
                            // Print a stacktrace to give some clue what it going wrong:
                            // we cannot do much more at this point here...
                            e.printStackTrace();
                        }
                    });
            }
        }

        private void dropOrWipeCollections(BundleContext ctx, ServiceReference<MongoDBService> ref) {
            MongoDBService srv = ctx.getService(ref);
            try {
                if (srv == null) {
                    // We're done already: service is no longer with us...
                    return;
                }

                MongoDatabase db = srv.getDatabase();
                ((Iterable<String>) db.listCollectionNames()).forEach(name -> dropOrWipeCollection(db, name));
            }
            finally {
                ctx.ungetService(ref);
            }
        }
    }

    public static final String PID = "org.amdatu.mongo";
    public static final String SYSTEM_INDEXES = "system.indexes";
    public static final String UNIQUE = "unique";
    public static final String SPARSE = "sparse";
    public static final String NAME = "name";
    public static final String KEY = "key";

    private static final AtomicBoolean SHUTDOWN_HOOK_CREATED = new AtomicBoolean();
    private static final AtomicReference<String> TEMP_DB_NAME = new AtomicReference<>();

    private final Set<String> m_excludedCollections;
    private final Set<String> m_wipedCollections;
    private boolean m_createTempDB;
    private String m_host;
    private String m_username;
    private String m_password;
    private String m_port;
    private String m_alias;
    private int m_duration;
    private TimeUnit m_unit;

    private ConfigurationStep m_delegate;

    public MongoConfigurationStep() {
        // Default to the old behaviour of creating a temp DB before each test case...
        m_createTempDB = true;

        // Default timeout of 5 seconds...
        m_duration = 5;
        m_unit = TimeUnit.SECONDS;

        m_excludedCollections = new HashSet<>();
        m_excludedCollections.add(SYSTEM_INDEXES);

        m_wipedCollections = new HashSet<>();
    }

    static String createAliasFilterString(String dbName) {
        return String.format("(&(%s=%s)(alias=%s))", OBJECTCLASS, MongoDBService.class.getName(), dbName);
    }

    private static boolean isEmpty(String s) {
        return s == null || s.equals("");
    }

    private static void setIfNotEmpty(Dictionary<String, Object> props, String key, String value) {
        if (!isEmpty(value)) {
            props.put(key, value);
        }
    }

    /**
     * Adds a collection name that should not be dropped or wiped.
     *
     * @param name
     *        the name of the collection that should be remain untouched.
     * @return this configuration step, never <code>null</code>.
     * @see #setWipeOrDropCollections()
     */
    public MongoConfigurationStep addExcludedCollection(String name) {
        m_excludedCollections.add(name);
        return this;
    }

    /**
     * Adds a collection name that should not be dropped but instead be wiped.
     *
     * @param name
     *        the name of the collection that should be wiped instead of
     *        dropped.
     * @return this configuration step, never <code>null</code>.
     * @see #setWipeOrDropCollections()
     */
    public MongoConfigurationStep addWipedCollection(String name) {
        m_wipedCollections.add(name);
        return this;
    }

    @Override
    public void apply(Object testCase) {
        if (m_createTempDB) {
            m_delegate = new TempDBConfigurationStep();
        }
        else {
            m_delegate = new WipeDBConfigurationStep();
        }
        m_delegate.apply(testCase);
    }

    @Override
    public void cleanUp(Object testCase) {
        if (m_delegate != null) {
            m_delegate.cleanUp(testCase);
            m_delegate = null;
        }
    }

    /**
     * Configures this step to create a temporary database for each test case.
     *
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setCreateTemporaryDatabase() {
        m_createTempDB = true;
        return this;
    }

    /**
     * Sets the host that MongoDBService should connect to.
     *
     * @param host
     *        the host to connect to.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setHost(String host) {
        m_host = host;
        return this;
    }

    /**
     * Sets the password that MongoDBService should use to connect.
     *
     * @param password
     *        the password to connect with.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setPassword(String password) {
        m_password = password;
        return this;
    }

    /**
     * Sets the alias that MongoDBService should use to connect.
     *
     * @param alias
     *        the alias to use for the MongoDB service.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setAlias(String alias) {
        m_alias = alias;
        return this;
    }

    /**
     * Sets the port that MongoDBService should connect to.
     *
     * @param port
     *        the port to connect to.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setPort(int port) {
        return setPort(Integer.toString(port));
    }

    /**
     * The time to wait for during the cleanup before the mongoDB service should go
     * away.
     * <p>
     * This timeout is only used when {@link #setCreateTemporaryDatabase()} is used
     * (which is the default)!
     * </p>
     *
     * @param duration
     *        the duration to use;
     * @param unit
     *        the time unit to use, cannot be <code>null</code>.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setTimeout(int duration, TimeUnit unit) {
        m_duration = duration;
        m_unit = unit;
        return this;
    }

    /**
     * Sets the username that MongoDBService should use to connect.
     *
     * @param username
     *        the username to connect withpassword.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setUsername(String username) {
        m_username = username;
        return this;
    }

    /**
     * Configures this step to not create a temporary database but drop or wipe the
     * collections for each test case.
     *
     * @return this configuration step, never <code>null</code>.
     * @see #addExcludedCollection(String)
     * @see #addWipedCollection(String)
     */
    public MongoConfigurationStep setWipeOrDropCollections() {
        m_createTempDB = false;
        return this;
    }

    final Dictionary<String, Object> createConfig(String dbName) {
        Dictionary<String, Object> props = new Hashtable<>();
        setIfNotEmpty(props, "dbName", dbName);
        setIfNotEmpty(props, "host", m_host);
        setIfNotEmpty(props, "port", m_port);
        setIfNotEmpty(props, "username", m_username);
        setIfNotEmpty(props, "password", m_password);
        setIfNotEmpty(props, "alias", m_alias);
        return props;
    }

    /**
     * Sets the port that MongoDBService should connect to.
     *
     * @param port
     *        the port to connect to.
     * @return this configuration step, never <code>null</code>.
     */
    final MongoConfigurationStep setPort(String port) {
        m_port = port;
        return this;
    }
}
