/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.impl;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

public class MongoDBServiceFactory implements ManagedServiceFactory {
    public static final String ALIAS = "alias";
    private final Map<String, Component> m_Components = new ConcurrentHashMap<>();
    private volatile LogService m_logService;
    private volatile DependencyManager m_dependencyManager;

    @Override
    public String getName() {
        return "org.amdatu.mongo";
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        if (!m_Components.containsKey(pid)) {
           try {
                String mongoURI = getProperty(properties, "mongoURI", null);
                String dbName = getProperty(properties, "dbName", "test");
                String alias = getProperty(properties, ALIAS, null);
                if (alias == null) {
                    throw new ConfigurationException(ALIAS, "alias is required.");
                }
                MongoClient mongo;
                
                if (mongoURI != null) {
                    ConnectionString connectionString = new ConnectionString(mongoURI);
                    mongo = MongoClients.create(connectionString);
                    dbName = connectionString.getDatabase();
                }
                else {
                    MongoClientSettings mongoSettings = createMongoSettings(properties, dbName);
                    mongo = MongoClients.create(mongoSettings);
                }

                MongoDBServiceImpl instance = new MongoDBServiceImpl(mongo, dbName);

                Properties serviceProperties = new Properties();
                serviceProperties.put("dbName", dbName);
                serviceProperties.put(ALIAS, alias);
                Component component = m_dependencyManager.createComponent()
                    .setInterface(MongoDBService.class.getName(), serviceProperties).setImplementation(instance);
                m_dependencyManager.add(component);
                m_Components.put(pid, component);
                m_logService.log(LogService.LOG_INFO, "Created MongoDB service for pid '" + pid + "'");

            }
            catch (MongoException e) {
                if (m_logService != null) {
                    m_logService.log(LogService.LOG_ERROR, "Error connecting to Mongo: " + e.getMessage(), e);
                }
                throw new RuntimeException(e);
            }
        }
    }
   
    private MongoCredential getMongoCredential(String username, String password, String dbName) {
        MongoCredential credential = null;

        if (username != null && !username.equals("")) {
            credential = MongoCredential.createCredential(username, dbName, password.toCharArray());
            m_logService.log(LogService.LOG_INFO, "Authenticated as '" + username + "'");
        }
        return credential;
    }
    
    private List<ServerAddress> getServerAddresses(String host, int port) {
        List<ServerAddress> addresses = new ArrayList<>();
        if (host.contains(",")) {
            String[] hosts = host.split(",");
            for (String hostUrl : hosts) {
                ServerAddress serverAddress;
                if (hostUrl.contains(":")) {
                    String[] hostUrlParts = hostUrl.split(":");
                    port = Integer.parseInt(hostUrlParts[1]);
                    serverAddress = new ServerAddress(hostUrlParts[0], port);
                }
                else {
                    serverAddress = new ServerAddress(hostUrl);
                }
                addresses.add(serverAddress);
            }
        }
        else {
            addresses.add(new ServerAddress(host, port));
        }
        return addresses;
    }

    private MongoClientSettings createMongoSettings(Dictionary<String, ?> properties, String dbName) 
            throws ConfigurationException {
        String host = getProperty(properties, "host", "localhost");
        int port = getProperty(properties, "port", 27017);
        String username = getProperty(properties, "username", null);
        String password = getProperty(properties, "password", null);
        int connectionsPerHost = getProperty(properties, "connectionsPerHost", 100);
        int connectTimeout = getProperty(properties, "connectTimeout", 10000);
        int maxWaitTime = getProperty(properties, "maxWaitTime", 120000);
        int socketTimeout = getProperty(properties, "socketTimeout", 0);
        String readPreference = getProperty(properties, "readPreference", "primary");
        String writeConcern = getProperty(properties, "writeConcern", "ACKNOWLEDGED");
        int retryWrites = getProperty(properties, "retryWrites", 1);

        MongoClientSettings.Builder builder = MongoClientSettings.builder()
            .retryWrites(retryWrites > 0)
            .applyToConnectionPoolSettings(cpsBuilder -> cpsBuilder
                .maxSize(connectionsPerHost)
                .maxWaitTime(maxWaitTime, TimeUnit.MILLISECONDS))
            .applyToSocketSettings(ssBuilder -> ssBuilder
                .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(socketTimeout, TimeUnit.MILLISECONDS))
            .applyToClusterSettings(csBuilder -> csBuilder
                .hosts(getServerAddresses(host, port)));
        
        MongoCredential credential = getMongoCredential(username, password, dbName);
        if (credential != null) {
            builder = builder.credential(credential);
        }
        setReadPreference(readPreference, builder);
        setWriteConcern(writeConcern, builder);
        
        return builder.build();
    }

    private void setWriteConcern(String writePreference, MongoClientSettings.Builder builder)
        throws ConfigurationException {

        if ("ACKNOWLEDGED".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.ACKNOWLEDGED);
        }
        else if ("JOURNALED".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.JOURNALED);
        }
        else if ("MAJORITY".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.MAJORITY);
        }
        else if ("UNACKNOWLEDGED".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.UNACKNOWLEDGED);
        }
        else if ("W1".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.W1);
        }
        else if ("W2".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.W2);
        }
        else if ("W3".equalsIgnoreCase(writePreference)) {
            builder.writeConcern(WriteConcern.W3);
        }
        else {
            if (m_logService != null) {
                m_logService.log(LogService.LOG_ERROR, "Error setting Mongo Write Concern '" + writePreference + "'");
            }
            throw new ConfigurationException("writeConcern", "Invalid writeConcern value'" + writePreference + "'");
        }
    }

    private void setReadPreference(String readPreference,
        MongoClientSettings.Builder builder) throws ConfigurationException {
        if ("primary".equals(readPreference)) {
            builder.readPreference(ReadPreference.primary());
        }
        else if ("primaryPreferred".equals(readPreference)) {
            builder.readPreference(ReadPreference.primaryPreferred());
        }
        else if ("secondary".equals(readPreference)) {
            builder.readPreference(ReadPreference.secondary());
        }
        else if ("secondaryPreferred".equals(readPreference)) {
            builder.readPreference(ReadPreference.secondaryPreferred());
        }
        else if ("nearest".equals(readPreference)) {
            builder.readPreference(ReadPreference.nearest());
        }
        else {
            if (m_logService != null) {
                m_logService.log(LogService.LOG_ERROR, "Error setting Mongo Read Preference");
            }
            throw new ConfigurationException("readPreference", "Invalid value for readPreference");
        }
    }

    @Override
    public void deleted(String pid) {
        if (m_Components.containsKey(pid)) {
            Component instance = m_Components.get(pid);
            ((MongoDBServiceImpl) instance.getInstance()).close();

            // Potentially removed by a concurrent thread.
            if (instance != null) {
                m_dependencyManager.remove(instance);
            }

            m_Components.remove(pid);

            m_logService.log(LogService.LOG_INFO, "Removed MongoDB service for pid '" + pid + "'");
        }
    }

    private String getProperty(Dictionary<String, ?> properties, String key, String defaultValue) {
        Object value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return value.toString();
        }
    }

    private int getProperty(Dictionary<String, ?> properties, String key, int defaultValue) {
        Object value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return Integer.parseInt(value.toString());
        }
    }
}
